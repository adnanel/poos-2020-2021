import os

import cv2

# settings for datasets in general

master_path_to_dataset = "dataset"  # ** need to edit this **

# data location - training examples

DATA_training_path_neg = os.path.join(master_path_to_dataset, "Train/neg/")
DATA_training_path_saxophone = os.path.join(master_path_to_dataset, "train_cropped/saxophone/")
DATA_training_path_flute = os.path.join(master_path_to_dataset, "train_cropped/flute/")
DATA_training_path_violin = os.path.join(master_path_to_dataset, "train_cropped/violin/")

# data location - testing examples

DATA_testing_path_neg = os.path.join(master_path_to_dataset, "Test/neg/")
DATA_test_path_saxophone = os.path.join(master_path_to_dataset, "test_cropped/saxophone/")
DATA_test_path_flute = os.path.join(master_path_to_dataset, "test_cropped/flute/")
DATA_test_path_violin = os.path.join(master_path_to_dataset, "test_cropped/violin/")

# size of the sliding window patch / image patch to be used for classification
# (for larger windows sizes, for example from selective search - resize the
# window to this size before feature descriptor extraction / classification)

DATA_WINDOW_SIZE = [64, 128]

# the maximum left/right, up/down offset to use when generating samples for training
# that are centred around the centre of the image

DATA_WINDOW_OFFSET_FOR_TRAINING_SAMPLES = 3

# number of sample patches to extract from each negative training example

DATA_training_sample_count_neg = 10

# number of sample patches to extract from each positive class training example

DATA_training_sample_count_saxophone = 5
DATA_training_sample_count_flute = 5
DATA_training_sample_count_violin = 5

# class names - N.B. ordering of 0, 1, 2, 3 for neg, saxophone, flute and violin = order of paths

DATA_CLASS_NAMES = {
    "other": 0,
    "saxophone": 1,
    "flute": 2,
    "violin": 3
}

DATA_CLASS_NAMES_FROM_INT = {
    0: "other",
    1: "saxophone",
    2: "flute",
    3: "violin"
}

# settings for HOG approaches

HOG_SVM_PATH = "svm_hog.xml"

HOG_SVM_kernel = cv2.ml.SVM_LINEAR  # see opencv manual for other options
HOG_SVM_max_training_iterations = 500  # stop training after max iterations
