from sliding_window import *
from utils import *

directory_to_cycle = "dataset/final_validation/"
results_path = "dataset/results"

show_scan_window_process = True

# load SVM from file

try:
    svm = cv2.ml.SVM_load(params.HOG_SVM_PATH)
except:
    print("Missing files - SVM!")
    print("-- have you performed training to produce these files ?")
    exit()

# print some checks

print("svm size : ", len(svm.getSupportVectors()))
print("svm var count : ", svm.getVarCount())

# process all images in directory (sorted by filename)

for filename in sorted(os.listdir(directory_to_cycle)):

    # if it is a JPG file

    if '.jpg' in filename or '.jpeg' in filename:
        print(os.path.join(directory_to_cycle, filename))

        # read image data

        img = cv2.imread(os.path.join(directory_to_cycle, filename), cv2.IMREAD_COLOR)

        # make a copy for drawing the output
        img = cv2.resize(img, (300, 500), interpolation=cv2.INTER_AREA)

        output_img = img.copy()

        # for a range of different image scales in an image pyramid

        current_scale = -1
        detections = []
        detected_labels = []
        rescaling_factor = 1.25

        # for each re-scale of the image

        for resized in pyramid(img, scale=rescaling_factor):

            # at the start our scale = 1, because we catch the flag value -1

            if current_scale == -1:
                current_scale = 1

            # after this rescale downwards each time (division by re-scale factor)

            else:
                current_scale /= rescaling_factor

            rect_img = resized.copy()

            # if we want to see progress show each scale

            if show_scan_window_process:
                cv2.imshow('Current scale', rect_img)
                cv2.waitKey(10)

            # loop over the sliding window for each layer of the pyramid (re-sized image)

            window_size = params.DATA_WINDOW_SIZE
            step = math.floor(resized.shape[0] / 16)

            if step > 0:

                # for each scan window

                for (x, y, window) in sliding_window(resized, window_size, step_size=step):

                    # if we want to see progress show each scan window

                    if show_scan_window_process:
                        cv2.imshow('Current window', window)
                        key = cv2.waitKey(10)  # wait 10ms

                    # for each window region get the HOG feature descriptors

                    img_data = ImageData(window)
                    img_data.compute_hog_descriptor()

                    # generate and classify each window by constructing a HOG
                    # histogram and passing it through the SVM classifier

                    if img_data.hog_descriptor is not None:

                        print("Detecting with SVM ...")

                        retval, [result] = svm.predict(np.float32([img_data.hog_descriptor]))

                        print(result)

                        # if we get a detection, then record it

                        if result[0] == params.DATA_CLASS_NAMES["saxophone"] \
                                or result[0] == params.DATA_CLASS_NAMES["flute"] \
                                or result[0] == params.DATA_CLASS_NAMES["violin"]:
                            # store rect as (x1, y1) (x2,y2) pair

                            rect = np.float32([x, y, x + window_size[0], y + window_size[1]])

                            # if we want to see progress show each detection, at each scale
                            if show_scan_window_process:
                                cv2.rectangle(rect_img, (int(rect[0]), int(rect[1])), (int(rect[2]), int(rect[3])),
                                              (0, 255, 0), 2)
                                label = params.DATA_CLASS_NAMES_FROM_INT[result[0]]
                                detected_labels.append(label)
                                font = cv2.FONT_HERSHEY_DUPLEX
                                cv2.putText(rect_img, label, (int(rect[0]) + 5, int(rect[1]) + 20), font, 0.8,
                                            (0, 255, 0), 1)
                                cv2.imshow('current scale', rect_img)
                                cv2.waitKey(40)

                            rect *= (1.0 / current_scale)
                            detections.append(rect)

        # For the overall set of detections (over all scales) perform
        # non maximal suppression (i.e. remove overlapping boxes etc).

        detections, detection_labels = non_max_suppression_fast(np.int32(detections), detected_labels, 0.1)

        # finally draw all the detection on the original image

        for (rect, label) in zip(detections, detection_labels):
            cv2.rectangle(output_img, (rect[0], rect[1]), (rect[2], rect[3]), (0, 255, 0), 2)
            font = cv2.FONT_HERSHEY_DUPLEX
            cv2.putText(output_img, label, (int(rect[0]) + 5, int(rect[1]) + 20), font, 0.8, (0, 255, 0), 1)

        if not os.path.exists(results_path):
            os.makedirs(results_path)
        cv2.imwrite(os.path.join(results_path, filename), output_img)

        cv2.imshow('Detected objects', output_img)
        key = cv2.waitKey(200)  # wait 200ms
        if key == ord('x'):
            break

# close all windows

cv2.destroyAllWindows()
