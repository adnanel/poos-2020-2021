import os

import cv2
import numpy as np
import xlrd
from PIL import Image


def use_mask(img, mask):
    return cv2.bitwise_and(img, mask)


def crop_mask(imageName, newImageName, upRight, bottomLeft, imeDir):
    image = Image.open("dataset/Train/" + imeDir + "/" + imageName)
    path1 = 'dataset/train_cropped/'

    if not os.path.exists(path1):
        os.makedirs(path1)

    cropped = image.crop((bottomLeft[0], bottomLeft[1], upRight[0], upRight[1]))
    cropped.save(path1 + imeDir + "/" + newImageName)


def create_and_apply_mask(imageName, newImageName, upLeft, bottomRight, imeDir):
    img = cv2.imread("dataset/Train/" + imeDir + "/" + imageName)  # load original image
    mask = np.zeros(img.shape, dtype="uint8")  # create black image (mask)
    cv2.rectangle(mask, upLeft, bottomRight, (255, 255, 255), -1)  # draw white rectangle on the mask
    masked_img = use_mask(img, mask)
    path1 = 'dataset/masks/'

    if not os.path.exists(path1):
        os.makedirs(path1)

    if not os.path.exists(path1 + imeDir):
        os.makedirs(path1 + imeDir)

    cv2.imwrite(os.path.join(path1 + imeDir, newImageName), masked_img)


src = 'annotations.xlsx'
book = xlrd.open_workbook(src)
work_sheet = book.sheet_by_index(0)
num_rows = work_sheet.nrows
current_row = 0

saxophone_counter = np.full(40, 0, dtype=int)
flute_counter = np.full(40, 0, dtype=int)
violin_counter = np.full(40, 0, dtype=int)

while current_row < num_rows:
    bottomL = (int(work_sheet.cell_value(current_row, 0)), int(work_sheet.cell_value(current_row, 1)))
    bottomR = (int(work_sheet.cell_value(current_row, 2)), int(work_sheet.cell_value(current_row, 3)))
    upR = (int(work_sheet.cell_value(current_row, 4)), int(work_sheet.cell_value(current_row, 5)))
    upL = (int(work_sheet.cell_value(current_row, 6)), int(work_sheet.cell_value(current_row, 7)))

    imageName = work_sheet.cell_value(current_row, 8)
    name, ext = os.path.splitext(imageName)

    if 1 <= int(name) <= 40:
        new_saxophone_image_name = name + "_" + str(saxophone_counter[int(name) - 1]) + ext
        saxophone_dir_name = "saxophone"
        create_and_apply_mask(imageName, new_saxophone_image_name, upL, bottomR, saxophone_dir_name)
        crop_mask(imageName, new_saxophone_image_name, upR, bottomL, saxophone_dir_name)
        saxophone_counter[int(name) - 1] += 1
    elif 41 <= int(name) <= 80:
        new_flute_image_name = name + "_" + str(flute_counter[(int(name) % 40) - 1]) + ext
        flute_dir_name = "flute"
        create_and_apply_mask(imageName, new_flute_image_name, upL, bottomR, flute_dir_name)
        crop_mask(imageName, new_flute_image_name, upR, bottomL, flute_dir_name)
        flute_counter[(int(name) % 40) - 1] += 1
    else:
        new_violin_image_name = name + "_" + str(violin_counter[(int(name) % 40) - 1]) + ext
        violin_dir_name = "violin"
        create_and_apply_mask(imageName, new_violin_image_name, upL, bottomR, violin_dir_name)
        crop_mask(imageName, new_violin_image_name, upR, bottomL, violin_dir_name)
        violin_counter[(int(name) % 40) - 1] += 1

    current_row += 1
